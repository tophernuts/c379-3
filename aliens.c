#include "aliens.h"

void* make_alien(void* alien_things){
	struct alien* alien_info = (struct alien*) alien_things;
	int crash = 0;
	int i;
	while(alien_info->x < COLS){
		for(i = 0; i < WAIT_DIVISION; i++){
			if(pthread_mutex_trylock(&collision_mutex)){
				// Someone else has the mutex
			} else {
				// We got the mutex
				if(alien_info->hit) crash = 1;
				pthread_mutex_unlock(&collision_mutex);
				if(crash) break;
			}
			/*
			 * If we're here we either didn't get any mutex
			 * or we did, but their hasn't been a collision.
			 * Either way I am tired so lettuce go to sleep
			 */
			usleep(alien_info->delay/WAIT_DIVISION);
		}
		
		if(crash) break;
		
		pthread_mutex_lock(&alien_mutex);
		alien_info->x++;
		pthread_mutex_unlock(&alien_mutex);
		
		pthread_mutex_lock(&mx);
		mvaddstr(alien_info->y, alien_info->x-1, BLANK_ALIEN);
		mvaddstr(alien_info->y, alien_info->x, alien_info->ship);
		refresh();
		pthread_mutex_unlock(&mx);
		
		/* Check if we've hit the edge and do something
		 * about it*/
		 pthread_mutex_lock(&alien_mutex);
		 if(alien_info->x+5 >= COLS){
			if(alien_info->x == COLS-2){
				alien_info->ship = "<";
			} else if(alien_info->x == COLS-3){
				alien_info->ship = "<-";
			} else if(alien_info->x == COLS-4){
				alien_info->ship = "<--";
			} else if(alien_info->x == COLS-5){
				alien_info->ship = "<---";
			}
		}
		pthread_mutex_unlock(&alien_mutex);
	}
	
	if(crash){
		
	} else {
		/* Will be cancelled or something if we get hit */
		pthread_mutex_lock(&update_mutex);
		escaped_aliens++;
		score += SCORE_ALIEN_GONE;
		pthread_cond_signal(&update_cv);
		pthread_mutex_unlock(&update_mutex);
	}
	
	// We do this no matter what
	// Put the ship back to its original shape
	alien_info->ship = ALIEN;
	
	// Make it gone
	pthread_mutex_lock(&mx);
	mvaddstr(alien_info->y, alien_info->x, BLANK_ALIEN);
	refresh();
	pthread_mutex_unlock(&mx);
	
	// Set this thread to done
	pthread_mutex_lock(&alien_mutex);
	alien_info->done = 1;
	pthread_mutex_unlock(&alien_mutex);
	
	pthread_exit(0);
}
