Chris Hut Assignment 2 Design
=============================
November 29th, 2013

Enclosed in this document is an insight into the design of my implementation of assignment 3 as well as how to use it.

Notes about this document:
      - The words bullet and missile are used interchangeably throughout this document.
      - There are no guaruntees about the stability of the program if the window is resized during execution
      - Alien means the alien saucer object that flies from left to right

--------------
| How to run |
--------------
Creating application:
	In order to run my application you must first make it from the command line. This can be accomplished with the commad "make".

Running application:
	There are no command line arguments for my application, to run it just run the command "./shooter".

Using the application:
      The basic controls from the assignment specs have been implemented. They are:
      	  - Left arrow key to move left one space
	  - Right arrow key to move right one space
	  - Spacebar to fire a missile/bullet
	  - Captial letter Q to quit

      As an additional feature, I have implemented some cheat codes that can be accessed via the keyboard.
      In order to activate a cheat code, type the cheat without quotes, in its entirity, without pressing any other keys in between.
      	 - Note: This includes space bar and arrow keys.
	 - Note: While entering a cheat code, you will not be able to play the game until either you finish the cheat code or press more than invalid characters.

      List of Cheat Codes:
        Gameplay Cheats
	---------------
	"left" - sends launch site to leftmost side of screen
	"right" - sends launch site to rightmost side of screen
	"ammo" - adds 10 bullets/missiles to your total 
	  
        Color Changes
	-------------
	 "color{choice}" - changes the color of all objects (aliens, bullets, launch site, stats) to {choice}. {choice} can be:
	 		 - red, green, yellow, blue, magenta, cyan, white 
		   	- there is no space between color and {choice}

----------
| Design |
----------

Overview:
	Every seperate Ui Object is updating and being drawn to the screen on a seperate thread. The categories of UI Objects that I defined are:
	      Bullet, Alien, Stats, and the Launch Site.
	There are also a couple of background threads that provide some deal of efficiency and inter-thread communication. They are:
	      Collision checker, Alien manager. Main thread

Main Thread
-----------
The main thread (the thread that started with the application) performs all of the initialization of ncurses as well as creates other threads.
- The main thread creates the shooter and the alien thread, and then performs a join on the shooter thread, as once the shooter threads is finished we will quit the application.

Shooter/Launch Site Thread
--------------
Probably the thead that does the most amount of work. It initializes a fair amount of variables, starts some other threads, manags keypresses, and then keeps track of the launch site.
- Creates the launch site (struct shooter) object that represents the launch site and its location
- Initializes mutexes, initializes our array of bullets, and then draws it self at its starting location
- Creates the collision_check and stats_thread threads
- Enters an almost infinit loop.
  	 - During the execution of the application this thread will spend most of its time in this loop. We do these actions in this loop:
	   	  - Manage keyboard ouput
		    	   - Left/Right keys update the ship->move variable
			   - Space calls shoot_bullet() which, if there is any bullets and threads available, launches a bullet on a new thread with shoot_bullet().
			   - Cheats are handled
		  - Update ourselves if we've moved
If the ship needs to move, or if something about it is being changed (adding bullets with cheat), the ship_mutex is first locked to ensure that someone else isn't also accessing the ship at the same time,
   as we do pass the ship pointer to multiple different threads


Alien Manager Thread
--------------------
Another important thread, this thread monitors the threads that are available to become aliens, creates them with random speeds, delay, and rows, and keeps track of the total number of aliens that have launched.
- We allocate MAX_ALIENS threads, which will be reused throughout the run of this application. This means that there is a maximum of MAX_THREADS aliens on the screen at one time
- We check if there is an alien thread available, and if there is, we set up a new alien object, and launch it on a thread.
     - The delay between alien launches as well as speed of the aliens when in flight are based off a random number as well as the number of aliesn that have already been launched, which creates a small implementation of\        levels or stages.
- If there is no thread to launch an alien available, we will search through all of the current alien object until we find one that has completed and we can reuse.
  -This is done by searching through our array of aliens and checking each of their done values. If one is set, this alien has completed, (it was either hit by a buller or went across the screen), and we can reuse its thread. This is done by resetting all of the alien objects parameters to be initial values.

While searching through the alien objects, before we try and read any of the values of a particular alien, we call lock on the alien_mutex to ensure that no one is currently writing to this alien.

Alien Threads
-------------
These threads update the position of an alien and draw them to the screen.
- We perform the check if we have been hit as well as delay before moving in one small algorithm.
     - Instead of sleeping for the entire time that we are told to, we split this sleep up into	WAIT_DIVISION divisions. After each of the smaller sleeps, we check if we have been hit. Instead of doing a normal pthread_mutex_lock(), we perform a pthread_mutex_trylock(), as we don't want to be waiting on that mutex, and besides, even if we don't get the mutex during one check, we still have (WAIT_DIVISIONS-1) other tries. By splitting up the sleep into multiple smaller sleeps, we have added multiple checks if we have been hit. This improves the flow of the application, as if we are sleeping for the entire sleep period in a row and get hit, we still may be sleeping before we even check if we have been hit.
- If we haven't been hit, we one space to the right every loop iteration until we are off of the screen.
- Once we are off of the screen, we clear the screen, and let everyone else know that we have been finished and the way that we finished (killed or flew off of screen)

Throughout this threads before updating and reading values we lock one of the following semaphores (alien_mutex, collision_mutex, update_mutex).

Bullet Thread
-------------
The bullet thread is almost identical to the alien thread as they perform the same actions:
    - move, check if we've been hit, update stuff when we've been hit

Collision Thread
----------------
This thread loops through all bullets and aliens and checks if the intersect.
- Before checking each of the variables, we lock on whichever semaphore represents whichever object we are currently checking.
- Before updating the hit/done variables representing if an object is done, and if the reason it is done is that it has been hit we lock the collision_mutex
- In an effort to improve performance we only check if a bullet, alien pair have collided if:
     - their done value is not set (they are still moving)
     - if the bullet is in one of the valid alien rows, as this is the only place where we can have a collision

This thread uses the collision_mutex, update_mutex, alien_mutex, and bullet_mutex to ensure all operations are thread safe

Stats Thread
------------
The stats thread's only purpose is to update the stats of the application based on the current state of the application.
This is done by using a condition variable so that we do not have to constantly be writing to the screen if nothing has changed.
The stats are: users score, remaining bullets, and aliens that have escaped.
- This method just sits on the condition variable until one of those variables have been updated, and the updating entity also does a signal on the condition variable:
       - Score: +100 for hitting an alien, -1 for missing a bullet, -5 for an escaped alien.
       	 - This variable can be updated from collision checker, bullet thread, or alien thread, all or which who update the condition variable when needed.
       - Bullets: +1 bullet for hitting an alien, 10 for the ammo cheat, -1 for bullet shot
       	 - This variable is updated from the shooter thread (cheats and bullets shot) and from the collision thread when a bullet has hit an alien. In all cases the condition variable is signaled.
       - Escaped aliens: +1 whenever an alien has escaped from the right side of the screen
       	 - Only updated by alien thread. Signal is used.

There is lots of inter-thread communication throughout this application, all the critical parts have been protected by semaphores. Although this decreases the performance of the application, it is much rather prefered than the randomness that exists without semaphores.
