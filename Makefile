all: shooter

shooter: clean shooter.o aliens.o bullet.o
	gcc shooter.o aliens.o bullet.o $(LIBS) -o shooter

bullet.o:
	gcc -c -g bullet.c

aliens.o:
	gcc -c -g aliens.c

shooter.o:
	gcc -c -g shooter.c

clean:
	rm -rf *o shooter

LIBS = -lpthread -lcurses
