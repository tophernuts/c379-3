#ifndef BULLET_F
#define BULLET_F

#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <unistd.h>
#include <string.h>

//#define BULLET_SPEED 10000
#define BULLET_SPEED 50000
#define BULLET_START_LOCATION LINES-3
#define BULLET '^'
#define BLANK ' '
#define SCORE_MISSED_BULLET -1
#define WAIT_DIVISION 100

extern pthread_mutex_t mx;
extern pthread_mutex_t bullet_mutex;
extern pthread_mutex_t collision_mutex;
extern pthread_mutex_t update_mutex;
extern pthread_cond_t update_cv;

extern int score;

struct bullet{
	/*
	 * The X coordinate of the bullet. This never changes once it has 
	 * been set.
	 */
	int x;
	/*
	 * The y coordinate of the bullet. This changes (decreases as we're
	 * moving up) as the bullet progresses.
	 */
	int y;
	/*
	 * The delay of the bullet. Bullets only move in the negative y
	 * direction (up). This probably shouldn't change after being set.
	 * Sets the speed of the bullet as each thread will wait
	 * delay microseconds before performing any more cursing
	 */
	int delay;
	
	/* 
	 * Flag that indicates if this bullet has hit an alien or not
	 */
	int hit;
	
	/*
	 * Flag that indicates that bullet is done - lets hope that
	 * unititialized variables in C are 0.
	 */
	int done;

};

void* create_bullet(void *);

#endif
