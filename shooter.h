#include <stdlib.h>
#include <stdio.h>
#include <curses.h>
#include <pthread.h>
#include <unistd.h>
#include <assert.h>

#include "aliens.h"
#include "bullet.h"

#define SHOOTER_START_BULLETS 50
#define MAX_ALIENS 10
#define MAX_BULLETS 20
#define ALIEN_ROWS 10
#define MAX_ESCAPED 1500

#define SCORE_ALIEN_HIT 100

#define QUIT_GAME 1
#define LOST_GAME 0
#define NAME_MAX 10

//#define TIME 	100000

/* Ship stuff */
#define CLEAR ' '
#define SHOOTER '|'

/* Global Mutex */
pthread_mutex_t mx;
pthread_mutex_t alien_mutex;
pthread_mutex_t bullet_mutex;
pthread_mutex_t escape_mutex;
pthread_mutex_t ship_mutex;
pthread_mutex_t collision_mutex;
pthread_mutex_t update_mutex;
pthread_mutex_t status_mutex;

/* Update Condition variable */
pthread_cond_t update_cv;

/* Array of threads for Bullets */
pthread_t bullets[MAX_BULLETS];
/* Thread that controls disco mode */
pthread_t disco_thread;
/* Flag if we already have disco mode on */
int is_disco_on = 0;

/* Counter for next bullet to be shot, should be replaces with a bitmap
 * or some other data type than can keep track of whats good
 */
int current_bullet = 0;
/* Array of bullet structs for threads */
static struct bullet bullet_struct[SHOOTER_START_BULLETS];
int bullet_count = 0;
int bullet_thread_available = 0;
int next_bullet_thread = 0;

/* User's current score */
int score = 0;
/* Number of aliens hit */
int aliens_hit = 0;
/*
 * Status of completion:
 * QUIT_GAME - user quit the game
 * LOST_GAME - user lost the game
 */
int status;

int wave = 0;

/* Array of alien structs for threads */
static struct alien alien_stuff[MAX_ALIENS];

/* Stats thread */
pthread_t stats_thread;
/* Collision thread */
pthread_t collision_thread;
/* Shooter thread */
pthread_t shooter_thread;

int escaped_aliens;

/*
 * Defines what the shooter struct contains
 */ 
struct shooter{
	/* The x position of the shooter. Leftmost position is 0 */
	int x;
	/*
	 * The amount of bullets remaining for the shooter.
	 * If 0 shooter can't shoot
	 */
	int bullets;
	/*
	 * If we should move the shooter the next time we draw it.
	 * 1 means move to the right 1 place
	 * -1 means move to the left 1 place
	 * 0 means don't move him	 
	 */
	int move; 
};

void setup_curses();
void* shooter_stuff(void*);
void* alien_manage(void*);
void shoot_bullet(struct shooter*);
void update(struct shooter*);
void *update_stats(void*);
void* collision_check(void*);
int make_alien_time(int);
int get_alien_creation_delay(int);
void cheat(char, struct shooter*);
void start_up_message();
void end_message(int);
void display_highscores(float, int);
void wave_check(int);
void* disco_time(void*);
