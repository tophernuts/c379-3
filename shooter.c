/*
 * Shooter.c contains all the code that has anything to do with the
 * shooter of the bullets in the Space Invaders game.
 */

#include "shooter.h"

void setup_curses(){
	initscr();
	crmode();
	noecho();
	/*
	 * Allow characters inputed to be immediately available to
	 * program
	 */
	cbreak();
	// Sets up keyboard mappings
	keypad(stdscr, true);
	clear();
	
	//nodelay(stdscr, TRUE);
	immedok(stdscr, TRUE); // Enable immediate terminal refresh
	
	// Get this cursor out of here
	curs_set(0);
	
	if(has_colors() == TRUE){
	    start_color();
	    // default
	    init_pair(1, COLOR_WHITE, COLOR_BLACK);
	}
	
	//clearok(stdscr, TRUE);
}

/*
 * Code that runs in the shooter thread. Essentially does:
 * 	- Listens for keypresses
 *		- L/R: set the move int
 *		- SPACE: call shoot_bullet()
 * 		- Q: Quit game
 * 		- P: Pause game
 * 		- Cheats :)
 *	- Updates the number of bullets from Alien hits
 *	- Cleans up dead bullet threads
 */
void* shooter_stuff(void* stuff){
	
	struct shooter* ship = malloc(sizeof(struct shooter));
	if(!ship){
	    // Malloc failed this sucks
	    exit(1);
	}
	
	/* 
	 * We can't have less STARTING bullets than the max number of
	 * bullets available to shoot as we'll go over in the array
	 */
	assert(SHOOTER_START_BULLETS >= MAX_BULLETS);
	
	/* Initialize bullet mutex */
	pthread_mutex_t bullet_mutex = PTHREAD_MUTEX_INITIALIZER;
	/* Initialize ship mutex */
	pthread_mutex_t ship_mutex = PTHREAD_MUTEX_INITIALIZER;
	/* Initialize collision checker mutex */
	pthread_mutex_t collision_mutex = PTHREAD_MUTEX_INITIALIZER;

	int escaped_aliens = 0;
	
	/* 
	 * Don't need to protect this as there is only one thread
	 * accessing the ship at this time
	 */
	ship->x = COLS/2; /* This is middle of screen */
	ship->bullets = SHOOTER_START_BULLETS;
	ship->move = 0;
	
	if(pthread_create(&stats_thread, NULL, update_stats, ship)){
	    // Error creating stats thread
	    fprintf(stderr, "Error creating stats thread");
	    endwin();
	    exit(0);
	}
	
	/* We want some stats right now pls */
	pthread_mutex_lock(&update_mutex);
	pthread_cond_signal(&update_cv);
	pthread_mutex_unlock(&update_mutex);
	
	int c;
	
	// Gotta be somewhere 
	update(ship);
	
	/* Initialize all of the bullet stuff */
	int i;
	for(i = 0; i < MAX_BULLETS; i++){
	    /*
	     * These bullets haven't been shot yet
	     * So it should be impossible for them to collide
	     */
	    bullet_struct[i].done = 1;
	    bullet_struct[i].y = -1;
	}
	
	
	/* Now all ship operations on bullets have to be protected */
	if(pthread_create(&collision_thread, NULL, collision_check,
	    ship)){
	    // Error creating thread
	    fprintf(stderr, "error creating thread");
	    endwin();
	    exit(0);
	}

	int loop = 1;
	while(loop){
	    /* Check if we're done */
	    pthread_testcancel();
	    
	    c = getch();
	    // Handle user input if any
	    switch(c){
	        case KEY_LEFT: // Left keyboard arrow
	            pthread_mutex_lock(&ship_mutex);
	            ship->move = -1;
	            pthread_mutex_unlock(&ship_mutex);
	        break;
	        case KEY_RIGHT: // Right keyboard arrow
	            pthread_mutex_lock(&ship_mutex);
	            ship->move = 1;
	            pthread_mutex_unlock(&ship_mutex);
	        break;
	        case 32: // SPACE key
	            shoot_bullet(ship);
	        break;
	        case 'Q': // Quit game
	            loop = 0;
	        break;
	        case 'a':
	        case 'c':
	        case 'd':
	        case 'i':
	        case 'l':
	        case 'r':
	            cheat(c, ship);
	        break;
	        case 'P':
	            /*
	             * Stuff will still be able to update, but
	             * everything wants to redraw itself right
	             * after its updated, meaning that everything
	             * will be in the correct place for when we
	             * resume
	             */
	            pthread_mutex_lock(&mx);
	            mvprintw(LINES/2, COLS/2-3, "PAUSED");
	            while(getch() != 'P'){
	                ;
	            }
	            mvprintw(LINES/2, COLS/2-3, "      ");
	            pthread_mutex_unlock(&mx);
	        break;
	        default:
	        ship->move = 0;

	        } // End of switch
	        pthread_mutex_lock(&ship_mutex);
	        if(ship->move){
	            pthread_mutex_unlock(&ship_mutex);
	            update(ship);
	            ship->x += ship->move;
	            ship->move = 0;
	        } else pthread_mutex_unlock(&ship_mutex);
	}
	pthread_mutex_lock(&status_mutex);
	status = QUIT_GAME;
	pthread_mutex_unlock(&status_mutex);
}

void cheat(char c, struct shooter* ship){
	char a;
	switch(c){
	    case 'a':
	        if(getch() == 'm')
	        if(getch() == 'm')
	        if(getch() == 'o'){
	            /* Ammo cheat */
	            pthread_mutex_lock(&ship_mutex);
	            ship->bullets += 10;
	            pthread_mutex_unlock(&ship_mutex);
	            /* Update the stats */
	            pthread_mutex_lock(&update_mutex);
	            pthread_cond_signal(&update_cv);
	            pthread_mutex_unlock(&update_mutex);
	        }
	    break;
	    case 'c':
	        a = getch();
	        if(a == 'o'){
	            if(getch() == 'l')
	            if(getch() == 'o')
	            if(getch() == 'r'){
	                a = getch();
	                if(a == 'r'){
	                    if(getch() == 'e')
	                    if(getch() == 'd'){
	                        init_pair(1, COLOR_RED, COLOR_BLACK);
	                        pthread_mutex_lock(&mx);
	                        bkgd(COLOR_PAIR(1));
	                        pthread_mutex_unlock(&mx);
	                    }
	                } else if(a == 'g'){
	                    if(getch() == 'r')
	                    if(getch() == 'e')
	                    if(getch() == 'e')
	                    if(getch() == 'n'){
	                        init_pair(1, COLOR_GREEN, COLOR_BLACK);
	                        pthread_mutex_lock(&mx);
	                        bkgd(COLOR_PAIR(1));
	                        pthread_mutex_unlock(&mx);
	                    }
	                } else if(a == 'y'){
	                    if(getch() == 'e')
	                    if(getch() == 'l')
	                    if(getch() == 'l')
	                    if(getch() == 'o')
	                    if(getch() == 'w'){
	                        init_pair(1, COLOR_YELLOW, COLOR_BLACK);
	                        pthread_mutex_lock(&mx);
	                        bkgd(COLOR_PAIR(1));
	                        pthread_mutex_unlock(&mx);
	                    }
	                } else if(a == 'b'){
	                    if(getch() == 'l')
	                    if(getch() == 'u')
	                    if(getch() == 'e'){
	                        init_pair(1, COLOR_BLUE, COLOR_BLACK);
	                        pthread_mutex_lock(&mx);
	                        bkgd(COLOR_PAIR(1));
	                        pthread_mutex_unlock(&mx);
	                    }
	                } else if(a == 'm'){
	                    if(getch() == 'a')
	                    if(getch() == 'g')
	                    if(getch() == 'e')
	                    if(getch() == 'n')
	                    if(getch() == 't')
	                    if(getch() == 'a'){
	                        init_pair(1, COLOR_MAGENTA,
	                            COLOR_BLACK);
	                        pthread_mutex_lock(&mx);
	                        bkgd(COLOR_PAIR(1));
	                        pthread_mutex_unlock(&mx);
	                    }
	                } else if(a == 'c'){
	                    if(getch() == 'y')
	                    if(getch() == 'a')
	                    if(getch() == 'n'){
	                        init_pair(1, COLOR_CYAN, COLOR_BLACK);
	                        pthread_mutex_lock(&mx);
	                        bkgd(COLOR_PAIR(1));
	                        pthread_mutex_unlock(&mx);
	                    }
	                } else if(a == 'w'){
	                    if(getch() == 'h')
	                    if(getch() == 'i')
	                    if(getch() == 't')
	                    if(getch() == 'e'){
	                        init_pair(1, COLOR_WHITE, COLOR_BLACK);
	                        pthread_mutex_lock(&mx);
	                        bkgd(COLOR_PAIR(1));
	                        pthread_mutex_unlock(&mx);
	                    }
	                }
	            }
	        } else if(a == 'h'){
	            if(getch() == 'r')
	            if(getch() == 'i')
	            if(getch() == 's')
	            if(getch() == 't')
	            if(getch() == 'm')
	            if(getch() == 'a')
	            if(getch() == 's'){
	                init_pair(1, COLOR_RED, COLOR_GREEN);
	                pthread_mutex_lock(&mx);
	                bkgd(COLOR_PAIR(1));
	                pthread_mutex_unlock(&mx);
	            }
	        }
	    break;
	    case 'd':
	        if(getch() == 'i')
	        if(getch() == 's')
	        if(getch() == 'c')
	        if(getch() == 'o')
	        if(getch() == 'o')
	        a = getch();
	        if(a == 'n'){
	            if(is_disco_on){
	                // Do nothing
	            } else {
	                if(pthread_create(&disco_thread, NULL,
	                    disco_time, NULL)){
	                        /* Couldn't make disco thread */
	                fprintf(stderr, "Couldn't create disco thread");
	                    endwin();
	                    exit(0);
	                }
	                is_disco_on = 1;
	            }
	        } else if( a == 'f'){
	            if(getch() == 'f'){
	                if(is_disco_on){
	                    // Turn off disco
	                    is_disco_on = 0;
	                } else {
	                    // Do nothing
	                    pthread_cancel(disco_thread);
	                }
	            }
	        }
	    break;
	    case 'i':
	        if(getch() == 'm')
	        if(getch() == 'p')
	        if(getch() == 'o')
	        if(getch() == 's')
	        if(getch() == 's')
	        if(getch() == 'i')
	        if(getch() == 'b')
	        if(getch() == 'l')
	        if(getch() == 'e'){
	            /* Why would anyone do this? */
	            init_pair(2, COLOR_YELLOW, COLOR_YELLOW);
	            pthread_mutex_lock(&mx);
	            bkgd(COLOR_PAIR(2));
	            pthread_mutex_unlock(&mx);
	        }
	    break;
	    case 'l':
	        if(getch() == 'e')
	        if(getch() == 'f')
	        if(getch() == 't'){
	            /* Left side of screen cheat */
	            pthread_mutex_lock(&ship_mutex);
	            ship->move = 0 - ship->x;
	            pthread_mutex_unlock(&ship_mutex);
	        }
	    break;
	    case 'r':
	        if(getch() == 'i')
	        if(getch() == 'g')
	        if(getch() == 'h')
	        if(getch() == 't'){
	            /* Right side of screen cheat */
	            pthread_mutex_lock(&ship_mutex);
	            ship->move = COLS - ship->x - 1;
	            pthread_mutex_unlock(&ship_mutex);
	        }
	    break;
	}
}

void* disco_time(void* nothing){
	/* Let's have a disco! */
	// Set up all of the color profiles
	init_pair(1, COLOR_BLACK, COLOR_YELLOW);
	init_pair(2, COLOR_YELLOW, COLOR_BLACK);
	init_pair(3, COLOR_RED, COLOR_GREEN);
	init_pair(4, COLOR_BLUE, COLOR_YELLOW);
	init_pair(5, COLOR_MAGENTA, COLOR_CYAN);
	init_pair(6, COLOR_MAGENTA, COLOR_BLUE);
	init_pair(7, COLOR_CYAN, COLOR_BLACK);
	init_pair(8, COLOR_WHITE, COLOR_RED);
	init_pair(9, COLOR_GREEN, COLOR_BLUE);
	init_pair(10, COLOR_BLACK, COLOR_GREEN);
	init_pair(11, COLOR_BLACK, COLOR_BLUE);
	init_pair(12, COLOR_CYAN, COLOR_BLACK);
	init_pair(13, COLOR_WHITE, COLOR_YELLOW);
	init_pair(14, COLOR_MAGENTA, COLOR_GREEN);
	init_pair(15, COLOR_GREEN, COLOR_WHITE);
	int i;
	while(1){
	    for(i = 1; i < 16; i++){
	        /* Check if we've been cancelled */
	        pthread_testcancel();
	        // Set the color!
	        pthread_mutex_lock(&mx);
	        bkgd(COLOR_PAIR(i));
	        pthread_mutex_unlock(&mx);
	        usleep(TIME);
	    }
	}
	
}

void main(int argc, char** argv){

	pthread_mutex_t mx = PTHREAD_MUTEX_INITIALIZER;
	pthread_mutex_t status_mutex = PTHREAD_MUTEX_INITIALIZER;

	setup_curses();
	
	start_up_message();
	
	/* Set up the condition variable nice and early */
	pthread_cond_t update_cv = PTHREAD_COND_INITIALIZER;

	if(pthread_create(&shooter_thread, NULL, shooter_stuff, NULL)){
	    // Error creating thread
	    fprintf(stderr, "error creating thread");
	    endwin();
	    exit(0);
	}
	pthread_t alien_thread;
	if(pthread_create(&alien_thread, NULL, alien_manage, NULL)){
	    // Error creating thread
	    fprintf(stderr, "error creating thread");
	    endwin();
	    exit(0);
	}

	pthread_join(shooter_thread, NULL);
	/* Change color back to normal */
	bkgd(COLOR_PAIR(1));
	
	pthread_cancel(alien_thread);
	pthread_cancel(collision_thread);
	pthread_cancel(stats_thread);
	/* Dirty hack to make aliens and bullets stop doing stuff */
	pthread_mutex_lock(&alien_mutex);
	pthread_mutex_lock(&bullet_mutex);
	end_message(status);
	endwin();
}


void start_up_message(){
	/*
	 * No threads have been created yet, so we can pretend that
	 * we're back in the good old single-threaded/what the hell is
	 * a mutex days.
	 */
	/* Welcome message */
	clear();
	mvprintw(10, 20, "Welcome to the alien shooter game.");
	mvprintw(11, 20, "Press any key to continue");
	getch();
	
	/* Instructions */
	clear();
	mvprintw(10, 20, "Use the arrow keys to move left and right");
	mvprintw(11, 20, "Use the spacebar to shoot missiles");
	mvprintw(12, 20, "Press any key to continue");
	getch();
	
	/* Start game or redo instructions */
	clear();
	mvprintw(10, 20, "Press r to view these instructions again,");
	mvprintw(11, 20, "press s to view the current high scores");
	mvprintw(12, 20, "or any other key to start the game");
	switch(getch()){

	    case 's':
	        // After viewing scores bring them back here
	        display_highscores(0, 0);
	    case 'r':
	        start_up_message();
	    break;
	}
	clear();
}

/*
 * Displays the appropriate end of game message based on if the user
 * died or quit the game.
 * 
 * We assume that there are no other threads running at this point, and
 * if there are, that they aren't doing anything to do with curses
 */
void end_message(int status){
	clear();
	char* str;
	/* Message based on status */
	if(status == QUIT_GAME){ // Player quit the game
	    switch(rand()%5){ // Randomized quit messages
	        case 0:
	            str = "Why would you quit, you were so close!";
	        break;
	        case 1:
	            str = "Good game bud!";
	        break;
	        case 2:
	str = "Gotta run? Too bad I'm not gonna save your progress!";
	        break;
	        case 3:
	        case 4:
	            str = "See ya next time champ";
	        break;
	        }
	} else if(status == LOST_GAME){ // Player lost the game
	    str = "The aliens have won, what were you thinking!";
	} else{ /* Invalid status */
	    mvprintw(10, 20, "Invalid status: %d", status);
	    return;
	}
	mvprintw(10, 20, "%s", str);
	/* Display stats about score and whatnot */
	mvprintw(11, 20, "Here are the stats for that round");
	mvprintw(12, 20, "Score: %d", score);
	mvprintw(13, 20, "Shots fired: %d", bullet_count);
	mvprintw(14, 20, "Aliens hit: %d", aliens_hit);
	/* Calculate accuracy */
	float acc;
	if(aliens_hit){
	    acc = (float) aliens_hit / (float) bullet_count*100;
	} else {
	    acc = 0;
	}
	mvprintw(15, 20, "Accuracy: %.2f%%", acc);
	mvprintw(16, 20, "Aliens escaped: %d", escaped_aliens);
	mvprintw(17, 20, "Press s to save your high score or any other key to quit");
	if(getch() == 's') display_highscores(acc, 1);
}

/*
 * Saves current users score as in high score file and displays other
 * peoples high scores as well.
 * */
void display_highscores(float acc, int save){
	clear();
	scrollok(stdscr, TRUE);
	FILE* f;
	int c,x,y;
	char* name;
	f = fopen("highscore", "a+");
	if(f){
	    // High score file exists, read it in and print it out
	    mvprintw(0, 0, "High Scores");
	    move(1, 0);
	    while((c = getc(f)) != EOF)
	        addch(c);
	        // Talk about your score if you have one:
	        if(save){
	            getyx(stdscr, y, x);
	            mvprintw((y+2), 0,
	"Your score: %d\tAccuracy: %.2f%% \tAliens hit: %d\tWave: %d",
	            score, acc, aliens_hit, wave);
	            name = malloc(NAME_MAX+1);
	            if(!name){
	                // malloc failed oh no
	                fclose(f);
	                return;
	            }
	            echo();
	            mvprintw((y+3), 0, "Please enter your name: ");
	            getnstr(name, NAME_MAX);
	            noecho();
	            /* Write our score to the file */
	            fprintf(f,
"Name: %s \tScore: %d\tAccuracy: %.2f%% \tAliens hit: %d\tWave: %d\n",
	            name, score, acc, aliens_hit, wave);
	        }
	    fclose(f);
	} else {
	    /* Error opening highscore file */
	    mvprintw(10, 20, "Error opening high score file");
	}
	getyx(stdscr, y, x);
	mvprintw((y+1), 0, "Press any key to %s.", save ? "quit" : "continue");
	getch();
	scrollok(stdscr, FALSE);
}

void* alien_manage(void* thread_stuff){
	// Create thread pool 
	pthread_t aliens[MAX_ALIENS];
	
	// Initialize alien mutex
	pthread_mutex_t alien_mutex = PTHREAD_MUTEX_INITIALIZER;
	/* Initialize escape mutext */
	pthread_mutex_t escape_mutex = PTHREAD_MUTEX_INITIALIZER;
	
	// Create counter of aliens
	int alien_count = 0;
	int alien_thread_available = 0;
	int next_alien_thread = 0;
	
	srand(getpid());
	
	// Set up all of the done variables to be 1, hit to 0
	int i;
	for(i = 0; i < MAX_ALIENS; i++){
	    alien_stuff[i].done = 1;
	    alien_stuff[i].hit = 0;
	    if(!(alien_stuff[i].ship = malloc(ALIEN_SHIP_SIZE))){
	        // Mallloc failed
	        fprintf(stderr, "Malloc failed");
	        exit(0);
	    }
	}
	// Loop and make more alien
	while(1){
	    pthread_testcancel();
	    if(alien_thread_available){
	        /* Sleep a random amount of time before going */
	        usleep(get_alien_creation_delay(alien_count));
	        /*
	         * We should be able to guaruntee that if we are here, then
	         * no one else will be touching these values
	         */

	        /* Do the wave */
	        wave_check(alien_count);

	        // We can create a new alien
	        alien_stuff[next_alien_thread].y = (rand()%ALIEN_ROWS);
	        // This is starting place of alien
	        alien_stuff[next_alien_thread].x = 0;
	        // This will be random based on alien_count
	        alien_stuff[next_alien_thread].delay =
	            make_alien_time(alien_count);
	        pthread_mutex_lock(&collision_mutex);
	        alien_stuff[next_alien_thread].done = 0;
	        alien_stuff[next_alien_thread].hit = 0;
	        pthread_mutex_unlock(&collision_mutex);

	        if(pthread_create(&aliens[next_alien_thread], NULL,
	            make_alien, &alien_stuff[next_alien_thread])){
	            // Error creating thread
	            fprintf(stderr, "Error creating alien thread");
	            endwin();
	            exit(0);
	        }

	        /* Detach created thread */
	        pthread_detach(aliens[next_alien_thread]);

	        alien_thread_available = 0;
	        alien_count++;
	    } else {
	        /*
	         * If we haven't already Created MAX_ALIENS threads, we for
	         * sure have threads available and we don't have to search
	         * for a done one
	         */
	        if(alien_count < MAX_ALIENS){
	            /* There is a thread available */
	            alien_thread_available = 1;
	            next_alien_thread = alien_count;
	                alien_stuff[alien_count].ship = ALIEN;
	        } else {
	            /* Check all threads to see if they're done*/
	            int done;
	            for(i = 0; i < MAX_ALIENS; i++){
	                /* Lock mutex */
	                pthread_mutex_lock(&alien_mutex);
	                done = alien_stuff[i].done;
	                pthread_mutex_unlock(&alien_mutex);
	                /* If an alien */
	                if(done){
	                    alien_thread_available = 1;
	                    next_alien_thread = i;
	                    /* We don't have to search no more*/
	                    break;
	                }
	            }
	        }
	    }
	}

}

/*
 * Tells the user what wave or level they are starting based on the
 * number of aliens that have been created.
 */
void wave_check(int alien_count){
	/*
	 * This could be done so much nicer with an array called waves
	 * but what you don't know can't hurt ya
	 */
	 
	/*
	 * No one else ever touches wave until we're done */
	switch(alien_count){
	    case 0:
	        wave = 1;
	    break;
	    case 20:
	        wave = 2;
	    break;
	    case 40:
	        wave = 3;
	    break;
	    case 60:
	        wave = 4;
	    break;
	    case 80:
	        wave = 5;
	    break;
	    case 100:
	        wave = 6;
	    break;
	    case 150:
	        wave = 7;
	    break;
	    case 200:
	        wave = 8;
	    break;
	    case 300:
	        wave = 9;
	    break;
	    case 400:
	        wave = 10;
	    break;
	    case 600:
	        wave = 11;
	    break;
	    case 1000:
	        wave = 12;
	    break;
	    case 2000:
	        wave = 13;
	    break;
	    case 5000:
	        wave = 14;
	    break;
	    case 10000:
	        wave = 15;
	    break;
	    default:
	        /*
	         * If you're not one of these numbers then you are in
	         * the middle of a wave.
	         */
	        return;
	}
	int time;
	pthread_mutex_lock(&mx);
	mvprintw(LINES/2, COLS/2-4, "WAVE: %d", wave);
	if(wave > 1)
	    mvprintw(LINES/2+2, COLS/2-4, "%d Point Bonus!", wave*1000);
	pthread_mutex_unlock(&mx);
	for(time = 5; time > 0; time--){
	    pthread_mutex_lock(&mx);
	    mvprintw(LINES/2+1, COLS/2-4, "Starts in: %d", time);
	    pthread_mutex_unlock(&mx);
	    usleep(500000);
	}
	pthread_mutex_lock(&mx);
	mvprintw(LINES/2+1, COLS/2-4, "            ");
	mvprintw(LINES/2, COLS/2-4, "        ");
	if(wave > 1)
	    mvprintw(LINES/2+2, COLS/2-4, "                  ");
	pthread_mutex_unlock(&mx);
	
	pthread_mutex_lock(&update_mutex);
	// Add the points
	if(wave > 1)
	score += wave*1000;
	pthread_mutex_unlock(&update_mutex);
	pthread_cond_signal(&update_cv);
	
	//pthread_mutex_unlock(&mx);
}

/*
 * Generates the proper delay between the creation of the last alien
 * and the creation of the next based on the current count of aliens
 */
int get_alien_creation_delay(int count){
	if(count <10){
	    return 1000000;
	} else if(count < 20){
	    return 500000;
	} else if(count < 40){
	    return 250000;
	} else if(count < 60){
	    return 180000;
	} else if(count < 100){
	    return 50000;
	} else{
	    return 0;
	}
}

/*
 * Creates a time delay for an alien based on the number of aliens
 * that have already been launched
 */
int make_alien_time(int count){

	int random = (1+rand()%3);
	if(count < 20){			// -10000
	    random = 1000*count*random;
	} else if(count < 40){		// -20000
	    random = 1000*count*random;
	} else if(count < 60){		// -30000
	    random = 1000*count*random;
	} else if(count < 80){		// -40000
	    random = 1000*count*random;
	} else if(count < 100){		// -5000
	    random = 100*count*random;
	} else if(count < 150){		// -75000
	    random = 1000*count*random;
	} else if(count < 200){		// -99000
	    random = 990*count*random;
	} else if(count < 300){
	    random = 200000;
	} else if(count < 400){
	    random = 210000;
	} else if(count < 600){
	    random = 220000;
	} else if(count < 1000){
	    random = 250000;
	} else if(count < 2000){
	    random = 260000;
	} else if(count < 5000){
	    random = 270000;
	} else if(count < 10000){
	    random = 280000;
	} else {
	    /* Good luck haha */	// At least -100000
	    random = 295000;
	}
	return TIME - random*4/3;
}

void* collision_check(void* nothing){
	struct shooter* ship = (struct shooter*) nothing;
	
	int bullet_iterator, alien_iterator;
	while(1){
	    pthread_testcancel();
	    /* 
	     * Iterate up to current_bullet and see if are currently
	     * intersecting with an alien
	     */
	    for(bullet_iterator = 0; bullet_iterator < MAX_BULLETS;
	        bullet_iterator++){

	        pthread_mutex_lock(&bullet_mutex);
	        if(bullet_struct[bullet_iterator].done ||
	            bullet_struct[bullet_iterator].hit){
	                pthread_mutex_unlock(&bullet_mutex);
	                continue;
	        } else {
	            pthread_mutex_unlock(&bullet_mutex);
	        }

	        /* To speed up checking because I'm an all around great
	         * guy, let's only look for a collision if the bullet is
	         * in a row that an alien could be, there is no point in 
	         * checking the other rows for bullets
	         */
	        pthread_mutex_lock(&bullet_mutex);
	        if(bullet_struct[bullet_iterator].y > ALIEN_ROWS){
	            // We can't possible hit an alien
	            pthread_mutex_unlock(&bullet_mutex);
	            continue; 
	        }
	        pthread_mutex_unlock(&bullet_mutex);
	
	        for(alien_iterator = 0; alien_iterator <
	            MAX_ALIENS; alien_iterator++){
	                pthread_mutex_lock(&alien_mutex);
	                // Check if alien or bullet is done
	                if(alien_stuff[alien_iterator].done ||
	                    alien_stuff[alien_iterator].hit){
	                        // Alien is done
	                        pthread_mutex_unlock(&alien_mutex);
	                        continue;
	                } else {
	                    pthread_mutex_unlock(&alien_mutex);
	                }
	        if(bullet_struct[bullet_iterator].y ==
	        alien_stuff[alien_iterator].y){
	            if(bullet_struct[bullet_iterator].x ==
	            alien_stuff[alien_iterator].x || 
	            (bullet_struct[bullet_iterator].x) ==
	            alien_stuff[alien_iterator].x + 1||
	            (bullet_struct[bullet_iterator].x) ==
	            alien_stuff[alien_iterator].x + 2||
	            (bullet_struct[bullet_iterator].x) ==
	            alien_stuff[alien_iterator].x + 3||
	            (bullet_struct[bullet_iterator].x) == 
	            alien_stuff[alien_iterator].x + 4){
	                // We have a collision
	                pthread_mutex_lock(&collision_mutex);
	                alien_stuff[alien_iterator].hit = 1;
	                bullet_struct[bullet_iterator].hit = 1;
	                pthread_mutex_unlock(&collision_mutex);
	                
	                pthread_mutex_lock(&ship_mutex);
	                ship->bullets++;
	                pthread_mutex_unlock(&ship_mutex);
	                
	                pthread_mutex_lock(&update_mutex);
	                score += SCORE_ALIEN_HIT;
	                pthread_cond_signal(&update_cv);
	                pthread_mutex_unlock(&update_mutex);
	                
	                aliens_hit++;
	                /*
	                 * If we have a collision break the loop
	                 * since the bullet has now dissappeared
	                 */
	                break;
	                } // end x check if
	            } // end y check if
	        } // end alien loop
	    } // end bullet loop
	} // end while loop 
}

void shoot_bullet(struct shooter* ship){
	int num_bullets;
	pthread_mutex_lock(&ship_mutex);
	num_bullets = ship->bullets;
	pthread_mutex_unlock(&ship_mutex);
	
	if(num_bullets <= 0){
	    /* We have no bullets we do nothing*/
	    return; // So we don't update stats
	} else if(bullet_count < MAX_BULLETS){
	    /* Afternoon delight */
	    /* We haven't yet used all of the bullets threads once */
	    next_bullet_thread = bullet_count;
	    bullet_thread_available = 1;
	} else if(bullet_thread_available == 0){
	    /* 
	     * No bullet threads available, lets check if any are actually
	     * free
	     */
	    int done,i;
	    for(i = 0; i < MAX_BULLETS; i++){
	        /* Lock mutex */
	        pthread_mutex_lock(&bullet_mutex);
	        done = bullet_struct[i].done;
	        pthread_mutex_unlock(&bullet_mutex);
	        if(done){
	            bullet_thread_available = 1;
	            next_bullet_thread = i;
	            /* No more searching */
	            break;
	        }
	    }
	}
	if(bullet_thread_available){
	    /* There is a bullet thread available let's use it */
	    pthread_mutex_lock(&ship_mutex);
	    bullet_struct[next_bullet_thread].x = ship->x;
	    pthread_mutex_unlock(&ship_mutex);
	    
	    bullet_struct[next_bullet_thread].y =
	    BULLET_START_LOCATION;
	    bullet_struct[next_bullet_thread].delay = BULLET_SPEED;
	    pthread_mutex_lock(&collision_mutex);
	    bullet_struct[next_bullet_thread].done = 0;
	    bullet_struct[next_bullet_thread].hit = 0;
	    pthread_mutex_unlock(&collision_mutex);
	    if(pthread_create(&bullets[next_bullet_thread], NULL,
	        create_bullet, &bullet_struct[next_bullet_thread])){
	            // Error creating thread
	            fprintf(stderr, "error creating bullet thread");
	        endwin();
	        exit(0);
	    }
	    /* Increment the bullet count and next_bullet_thread */
	    bullet_count++;
	    bullet_thread_available = 0;
	} else {
	    /* Can't shoot a bullet */
	    return;
	}
	/* Decrement number of bullets */
	pthread_mutex_lock(&update_mutex);
	ship->bullets--;
	pthread_cond_signal(&update_cv);
	pthread_mutex_unlock(&update_mutex);
}

void* update_stats(void* stats){
	/*
	 * We will only be called if something is changed
	 * so we can always update stuff here
	 */
	struct shooter* ship = (struct shooter*) stats;
	
	char str[COLS-1];
	int escaped_counter = 0;
	
	//pthread_mutex_lock(&ship_mutex);
	//int bullet_counter = ship->bullets;
	int bullet_counter = SHOOTER_START_BULLETS;
	//pthread_mutex_unlock(&ship_mutex);
	int our_score;
	
	
	while(escaped_counter < MAX_ESCAPED){
	    pthread_testcancel();
	    
	    pthread_mutex_lock(&update_mutex);
	    /* Block until something is updated and we can eat it */
	    pthread_cond_wait(&update_cv, &update_mutex);
	    /* Once we here someone wants to update */
	
	    /* Update our local values aww yeah */
	    escaped_counter = escaped_aliens;
	    pthread_mutex_lock(&ship_mutex);
	    bullet_counter = ship->bullets;
	    pthread_mutex_unlock(&ship_mutex);
	    our_score = score;
	
	    /* Gotta unlock the thread again cause its unlocked */
	    pthread_mutex_unlock(&update_mutex);
	    
	    /* Our values are updated lets write them to the screen */

	    pthread_mutex_lock(&mx);
	    mvprintw(LINES-1, 0,
	        "Score: %d\tRockets: %d\tEscaped Things: %d",
	        our_score,
	        bullet_counter, escaped_counter);
	        //1,2);
	    refresh();
	    pthread_mutex_unlock(&mx);
	    /* Check if we are out of bullets */
	    if(bullet_counter <= 0){
	        int i, done;
	        done = 1;
	        for(i = 0; i < MAX_BULLETS; i++){
	            pthread_mutex_lock(&bullet_mutex);
	            if(bullet_struct[i].done != 1){
	                pthread_mutex_unlock(&bullet_mutex);
	                done = 0;
	                break;
	            } else pthread_mutex_unlock(&bullet_mutex);
	        }
	        if(done) break;
	    }// endof if

	}// end of while
	pthread_mutex_lock(&status_mutex);
	status = LOST_GAME;
	pthread_mutex_unlock(&status_mutex);
	pthread_cancel(shooter_thread);
}

void update(struct shooter* ship){
	// We've gotta update our position of this buddy!
	int x, move;
	pthread_mutex_lock(&ship_mutex);
	x = ship->x;
	move = ship->move;
	pthread_mutex_unlock(&ship_mutex);
	if((x + move) < 0){
	    /* 
	     * We would be past the left side 0
	     * So we don't want to move it next time either
	     */
	    pthread_mutex_lock(&ship_mutex);
	    ship->move = 0;
	    pthread_mutex_unlock(&ship_mutex);
	} else if((x + move) >= COLS){
	    /*
	     * We would be past the right side MAX_COL-1
	     * So we don't want to move it next time either 
	     */
	    pthread_mutex_lock(&ship_mutex);
	    ship->move = 0;
	    pthread_mutex_unlock(&ship_mutex);
	} else { // We're okay
	    pthread_mutex_lock(&mx);
	    // Move to current place and clear it
	    mvaddch(LINES - 2, x, CLEAR);
	    // Move to new place and make it happen 
	    mvaddch(LINES - 2, x + move, SHOOTER);
	
	    /* We only need to update the screen if we changed it */
	    refresh();
	    pthread_mutex_unlock(&mx);
	}
}

