#ifndef ALIENS_H
#define ALIENS_H

#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <curses.h>

#define STARTING_SHIP_LOCATION 0
#define ALIEN "<--->"
#define BLANK_ALIEN "     "
#define ALIEN_SHIP_SIZE 5
#define SCORE_ALIEN_GONE -5
#define WAIT_DIVISION 100

//#define TIME 10000
#define TIME 500000

extern pthread_mutex_t mx;
extern pthread_mutex_t alien_mutex;
extern pthread_mutex_t escape_mutex;
extern pthread_mutex_t collision_mutex;

extern pthread_cond_t update_cv;
extern pthread_mutex_t update_mutex;

extern int escaped_aliens;
extern int score;

struct alien{
	/* The Y Coordinate of the alien ship */
	int y;
	/* The X coordiante of the alien ship */
	int x;
	/* The delay of the alien ship */
	int delay;
	/*
	 * Flag if alien is done.
	 * 0 if not done, non-zero if completed.
	 */
	int done;
	
	/* The shape of the ship */
	char* ship;
	
	/* 
	 * Flag that states whether or not this alien has been hit by a bullet
	 */
	 int hit;
};

void* make_alien(void*);

#endif
