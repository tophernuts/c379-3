/* 
 * bullet.c - Displays a bullet/missile that exists from launch (one
 * line above the shooter, until it hits the roof (dissappears/miss) or
 * hits an alien (dissappears/hit).
 */
 
#include "bullet.h"
 
 void* create_bullet(void* bullet_stuff){
	 struct bullet* bullet_info = (struct bullet*) bullet_stuff;
	 int crash = 0;
	 //while(0){
	 int i;
	 while(bullet_info->y > 0){
	    for(i = 0; i < WAIT_DIVISION; i++){
	        if(pthread_mutex_trylock(&collision_mutex)){
	            // Someone else hase the mutex
	        } else {
	            // we got it
	            crash = bullet_info->hit;
	            pthread_mutex_unlock(&collision_mutex);
	            if(crash) break;
	        }
	        /*
		 * If we're here then we either didn't get the
		 * mutex or we didn't crash.
		 * Goodnight.
		 */
		usleep(bullet_info->delay/WAIT_DIVISION);
	    }
	    pthread_mutex_lock(&collision_mutex);
	    if(bullet_info->hit){
	        crash = 1;
	    }
	    pthread_mutex_unlock(&collision_mutex);

	    if(crash) break;

	    /* Don't wanna do no math in a semaphore */
	    pthread_mutex_lock(&bullet_mutex);
	    bullet_info->y--;
	    pthread_mutex_unlock(&bullet_mutex);

	    pthread_mutex_lock(&mx);
	    /* Delete where we were */
	    mvaddch(bullet_info->y + 1, bullet_info->x, BLANK);
	    mvaddch(bullet_info->y, bullet_info->x, BULLET);
	    refresh();
	    pthread_mutex_unlock(&mx);

	}
	
	if(!crash){
	    pthread_mutex_lock(&update_mutex);
	    score += SCORE_MISSED_BULLET;
	    pthread_cond_signal(&update_cv);
	    pthread_mutex_unlock(&update_mutex);
	}
	
	/* Remove bullet */
	pthread_mutex_lock(&mx);
	mvaddch(bullet_info->y, bullet_info->x, BLANK);
	refresh();
	pthread_mutex_unlock(&mx);
	
	/* Bullet is done now */
	pthread_mutex_lock(&bullet_mutex);
	// This is making none of the bullets hit?
	bullet_info->done = 1;
	pthread_mutex_unlock(&bullet_mutex);
	
	pthread_exit(0); 
}
